from __future__ import annotations

import pygentest.random

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.state.abstract import State
from ansiblekit.cookbook.state.abstract import StateFactory
from ansiblekit.cookbook.state.file.abstract import StateFile
from ansiblekit.cookbook.state.file.impl import StateFileFactoryImpl
from ansiblekit.cookbook.state.impl import StateFactoryImpl
from ansiblekit.cookbook.state.impl import StateImpl


class StateFactoryMock(StateFactory):
    ORIGIN: StateFactory = StateFactoryImpl(
        state_file_factory=StateFileFactoryImpl()
    )

    @staticmethod
    def random() -> State:
        key = pygentest.random.gen_text()
        value = pygentest.random.gen_text()

        return StateImpl(
            state_file_factory=StateFileFactoryImpl(),
            vars={
                key: value
            },
            files={}
        )

    def create(self) -> State:
        return StateFactoryMock.ORIGIN.create()

    def load(self, facts: TreeNode) -> State:
        return StateFactoryMock.ORIGIN.load(facts)

    def create_and_update_file(self, path: str, file: StateFile) -> State:
        state = self.create()

        update_result = state.update_file(
            path=path,
            checksum=file.checksum(),
            ctime=file.ctime(),
            state=self.create()
        )

        return update_result.state()
