from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.action.close import CloseAction
from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.unix.action.impl import UnixAnsibleActionImpl


class ActionModule(UnixAnsibleActionImpl):
    def do_run(self, args: TreeNode, facts: TreeNode) -> ActionResult:
        action = CloseAction.create(
            args=args,
            remote_store_factory=self._remote_store_factory(),
            pre_cmd=self._pre_cmd(),
            name_factory=self._name_factory(),
            action_result_factory=self._result_factory()
        )

        return action.run(facts)
