from pygentree.node.abstract import TreeNode

from ansiblekit.ansible.action import Action
from ansiblekit.ansible.unix.action import AnsibleActionUnix
from ansiblekit.ansible.unix.host import UnixHost


class ActionModule(AnsibleActionUnix):
    def create_action(self, args: TreeNode, host: UnixHost) -> Action:
        raise Exception("Unsupported operation")

        # return UnixTemplateAction.create(
        #     args=args,
        #     host=host,
        #     pre_cmd=default.pre_cmd()
        # )
