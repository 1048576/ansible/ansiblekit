from __future__ import annotations

from typing import Dict

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.state.abstract import State
from ansiblekit.cookbook.state.abstract import StateFactory
from ansiblekit.cookbook.state.abstract import UpdateStateResult
from ansiblekit.cookbook.state.file.abstract import StateFile
from ansiblekit.cookbook.state.file.abstract import StateFileFactory


class StateImpl(State):
    KEY_VARS: str = "vars"
    KEY_FILES: str = "files"

    @staticmethod
    def load(
        facts: TreeNode,
        state_file_factory: StateFileFactory
    ) -> StateImpl:
        def _create_vars() -> Dict[str, str]:
            vars_node = facts.fetch_node(StateImpl.KEY_VARS)
            vars: Dict[str, str] = {}
            for k, v in vars_node.fetch_text_items():
                vars[k] = v

            return vars

        def _create_files() -> Dict[str, StateFile]:
            files_node = facts.fetch_node(StateImpl.KEY_FILES)
            files: Dict[str, StateFile] = {}
            for k, v in files_node.fetch_node_items():
                files[k] = state_file_factory.load(v)

            return files

        with (facts):
            return StateImpl(
                state_file_factory=state_file_factory,
                vars=_create_vars(),
                files=_create_files()
            )

    _state_file_factory: StateFileFactory
    _vars: Dict[str, str]
    _files: Dict[str, StateFile]

    def __init__(
        self,
        state_file_factory: StateFileFactory,
        vars: Dict[str, str],
        files: Dict[str, StateFile]
    ) -> None:
        self._state_file_factory = state_file_factory
        self._vars = vars
        self._files = files

    def __eq__(self, o: object) -> bool:
        if (not isinstance(o, StateImpl)):
            return False
        if (o._vars != self._vars):
            return False
        if (o._files != self._files):
            return False

        return True

    def facts(self) -> object:
        def _vars_facts() -> Dict[str, str]:
            return self._vars

        def _files_facts() -> Dict[str, object]:
            facts: Dict[str, object] = {}

            for path, file in self._files.items():
                facts[path] = file.facts()

            return facts

        return {
            StateImpl.KEY_VARS: _vars_facts(),
            StateImpl.KEY_FILES: _files_facts()
        }

    def vars(self) -> Dict[str, str]:
        return self._vars

    def files(self) -> Dict[str, StateFile]:
        return self._files

    def update_var(
        self,
        key: str,
        value: str,
        state: State
    ) -> UpdateStateResult:
        vars: Dict[str, str] = {
            key: value
        }

        return UpdateStateResult(
            changed=(state.vars().get(key) != value),
            state=StateImpl(
                state_file_factory=self._state_file_factory,
                vars={
                    **self.vars(),
                    **vars
                },
                files=self.files()
            )
        )

    def update_file(
        self,
        path: str,
        checksum: str,
        ctime: str,
        state: State
    ) -> UpdateStateResult:
        file = self._state_file_factory.create(checksum, ctime)
        files: Dict[str, StateFile] = {
            path: file
        }

        return UpdateStateResult(
            changed=(state.files().get(path) != file),
            state=StateImpl(
                state_file_factory=self._state_file_factory,
                vars=self.vars(),
                files={
                    **self.files(),
                    **files
                }
            )
        )


class StateFactoryImpl(StateFactory):
    _state_file_factory: StateFileFactory

    def __init__(
        self,
        state_file_factory: StateFileFactory
    ) -> None:
        self._state_file_factory = state_file_factory

    def create(self) -> State:
        return StateImpl(
            state_file_factory=self._state_file_factory,
            vars={},
            files={}
        )

    def load(self, facts: TreeNode) -> State:
        return StateImpl.load(
            state_file_factory=self._state_file_factory,
            facts=facts
        )
