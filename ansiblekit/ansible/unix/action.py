from ansiblekit.ansible.action import AnsibleAction
from ansiblekit.ansible.action import AnsibleExecutor
from ansiblekit.ansible.unix.host import UnixHost
from ansiblekit.ansible.unix.host import UnixHostImpl


class AnsibleActionUnix(AnsibleAction[UnixHost]):
    def create_host(self, executor: AnsibleExecutor) -> UnixHost:
        return UnixHostImpl(executor)
