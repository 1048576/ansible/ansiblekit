from typing import Dict

DEFAULT_TEMPLATE_OPTIONS: Dict[str, object] = {
    "lstrip_blocks": True,
    "trim_blocks": True
}
