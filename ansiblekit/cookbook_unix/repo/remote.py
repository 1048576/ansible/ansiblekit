from ansiblekit.ansible.unix.host import UnixHost
from ansiblekit.cookbook.repo.remote.abstract import RemoteRepo


class UnixRemoteRepo(RemoteRepo):
    host: UnixHost

    def __init__(self, host: UnixHost) -> None:
        self.host = host

    # def pull(self, name: Name) -> State:
    #     src = "/var/lib/cookbook/%s.yml" % (name.uri())

    #     try:
    #         result = self.host.slurp(src)
    #     except FileNotFoundException:
    #         raise CookbookNotExistsException(name)

    #     return StateImpl.load(
    #         facts=json.loads(result.content),
    #         state_file_factory=StateFileFactoryImpl()
    #     )

    # def push(self, name: Name, state: State) -> bool:
    #     self.host.mkdir(
    #         path="/var/lib/cookbook/",
    #         owner="root",
    #         group="root",
    #         mode="0666"
    #     )

    #     dest = "/var/lib/cookbook/%s.yml" % (name.uri())

    #     copy_result = self.host.copy(
    #         dest=dest,
    #         content=json.dumps(state.facts()) + "\n",
    #         owner="root",
    #         group="root",
    #         mode="0600"
    #     )

    #     return copy_result.changed()
