from __future__ import annotations

from typing import Dict

from pygenerr.expected import ExpectedException
from pygentree.node.abstract import TreeNode
from pygentree.node.impl import TreeNodeImpl

KEY_ANSIBLE_FACTS: str = "ansible_facts"
KEY_CHANGED: str = "changed"
KEY_FAILED: str = "failed"
KEY_MSG: str = "msg"


class ActionException(ExpectedException):
    result: ActionResult

    def __init__(self, result: ActionResult) -> None:
        super().__init__("Action failed")

        self.result = result


class FileNotFoundException(ActionException):
    def __init__(self, result: ActionResult) -> None:
        super().__init__(result)


class ActionResult(Dict[str, object]):
    @staticmethod
    def failed(e: ExpectedException) -> ActionResult:
        fields: Dict[str, object]

        if (isinstance(e, ActionException)):
            fields = e.result.copy()
        else:
            fields = {
                KEY_MSG: str(e)
            }

        fields[KEY_FAILED] = True

        return ActionResult(fields)

    @staticmethod
    def success(
        changed: bool,
        ansible_facts: Dict[str, str] = {}
    ) -> ActionResult:
        return ActionResult({
            KEY_CHANGED: changed,
            KEY_ANSIBLE_FACTS: ansible_facts
        })

    def __init__(self, fields: Dict[str, object]) -> None:
        super().__init__(fields)

    def changed(self) -> bool:
        return (self.get(KEY_CHANGED, False) is True)

    def msg(self) -> object:
        return self.get(KEY_MSG)

    def ansible_facts(self) -> TreeNode:
        return TreeNodeImpl.create(
            self.get(KEY_ANSIBLE_FACTS, {})
        )
