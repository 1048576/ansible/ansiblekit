import re

from typing import Pattern

from pygenerr.expected import ExpectedException
from pygentree.node.abstract import TreeNode
from pygentree.node.abstract import TreeNodeNotFoundException

from ansiblekit.cookbook.name.abstract import CookbookNotSpecifiedException
from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.name.abstract import NameFactory
from ansiblekit.core.facts import Facts


class NameImpl(Name):
    KEY_URI: str = "_ansiblekit_name_uri"
    EMPTY_URI: str = "empty.00000000-0000-0000-0000-000000000000"
    URI_PATTERN: Pattern[str] = re.compile(
        r"^[a-z][a-z0-9\-]*\.[0-9a-f]{8}\-(?:[0-9a-f]{4}\-){3}[0-9a-f]{12}$"
    )

    @staticmethod
    def empty() -> Name:
        return NameImpl(NameImpl.EMPTY_URI)

    @staticmethod
    def create(uri: str) -> Name:
        if (uri == NameImpl.EMPTY_URI):
            raise CookbookNotSpecifiedException()

        if (not NameImpl.URI_PATTERN.match(uri)):
            raise ExpectedException(
                msg_template="Invalid cookbook name [%s]",
                msg_vars=[uri]
            )

        return NameImpl(uri)

    @staticmethod
    def load(facts: TreeNode) -> Name:
        try:
            uri = facts.fetch_text(NameImpl.KEY_URI)
        except TreeNodeNotFoundException:
            raise CookbookNotSpecifiedException()

        return NameImpl.create(uri)

    _uri: str

    def __init__(self, uri: str) -> None:
        self._uri = uri

    def __eq__(self, o: object) -> bool:
        if (isinstance(o, NameImpl)):
            if (o._uri != self._uri):
                return False

            return True

        return False

    def uri(self) -> str:
        return self._uri

    def facts(self) -> Facts:
        return {
            NameImpl.KEY_URI: self._uri
        }


class NameFactoryImpl(NameFactory):
    @staticmethod
    def empty() -> Name:
        return NameImpl.empty()

    @staticmethod
    def create(uri: str) -> Name:
        return NameImpl.create(uri)

    @staticmethod
    def load(facts: TreeNode) -> Name:
        return NameImpl.load(facts)
