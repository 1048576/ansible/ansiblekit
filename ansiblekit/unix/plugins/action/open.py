from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.action.open import OpenAction
from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.unix.action.impl import UnixAnsibleActionImpl


class ActionModule(UnixAnsibleActionImpl):
    def do_run(self, args: TreeNode, facts: TreeNode) -> ActionResult:
        action = OpenAction.create(
            args=args,
            local_store_factory=self._local_store_factory(),
            remote_store_factory=self._remote_store_factory(),
            name_factory=self._name_factory(),
            action_result_factory=self._result_factory()
        )

        return action.run(facts)
