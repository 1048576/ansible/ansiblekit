from __future__ import annotations

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.action.unix.deploy import UnixDeployAction
from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.cmd.unix.deploy.abstract import UnixDeployCmd
from ansiblekit.cookbook.unix.host import UnixHost
from ansiblekit.core.action.abstract import Action
from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.cmd.unix.stat.abstract import UnixStatCmd


class AptRepositoryDeployCmd(UnixDeployCmd):
    _repo: str
    _host: UnixHost

    def __init__(self, repo: str, host: UnixHost) -> None:
        self._repo = repo
        self._host = host

    def run(self, path: str, owner: str, group: str, mode: str) -> None:
        return self._host.add_apt_repo(self._repo, path)


class AptRepositoryAction(Action):
    @staticmethod
    def create(
        args: TreeNode,
        host: UnixHost,
        pre_cmd: PreCmd,
        stat_cmd: UnixStatCmd,
        action_result_factory: ActionResultFactory
    ) -> Action:
        with (args):
            repo = args.fetch_text("repo")
            filename = args.fetch_text("filename")

        return AptRepositoryAction(
            repo=repo,
            filename=filename,
            host=host,
            pre_cmd=pre_cmd,
            stat_cmd=stat_cmd,
            action_result_factory=action_result_factory
        )

    _deploy_action: UnixDeployAction

    def __init__(
        self,
        repo: str,
        filename: str,
        host: UnixHost,
        pre_cmd: PreCmd,
        stat_cmd: UnixStatCmd,
        action_result_factory: ActionResultFactory
    ) -> None:
        self._deploy_action = UnixDeployAction(
            path="/etc/apt/sources.list.d/%s.list" % (filename),
            owner="root",
            group="root",
            mode="0644",
            pre_cmd=pre_cmd,
            deploy_cmd=AptRepositoryDeployCmd(
                repo=repo,
                host=host
            ),
            stat_cmd=stat_cmd,
            action_result_factory=action_result_factory
        )

    def run(self, facts: TreeNode) -> ActionResult:
        return self._deploy_action.run(facts)
