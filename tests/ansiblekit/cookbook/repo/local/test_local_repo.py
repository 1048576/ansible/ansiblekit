import os.path
import unittest

from pygenpath.resolver.impl import PathResolverImpl
from pygentest.generator import UniqueValueGenerator
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.cookbook.repo.errors import RepoAlreadyExistsException
from ansiblekit.cookbook.repo.errors import RepoNotExistsException
from ansiblekit.cookbook.repo.local.impl import LocalStoreFactoryImpl
from ansiblekit.cookbook.repo.local.impl import LocalStoreImpl
from ansiblekit.cookbook.state.abstract import StateFactory
from tests.ansiblekit.cookbook.name.mock import NameFactoryMock
from tests.ansiblekit.cookbook.state.mock import StateFactoryMock


class LocalStoreTest(unittest.TestCase):
    def test_purge(self) -> None:
        workdir = PathResolverImpl.temp()
        store = LocalStoreImpl(
            state_factory=StateFactory(),
            workdir=workdir
        )

        store.purge()

        self.assertFalse(
            expr=os.path.exists(workdir.pwd())
        )

    def test_create_repo_aleady_exists(self) -> None:
        name_factory = NameFactoryMock()
        state_factory = StateFactoryMock()
        local_store_factory = LocalStoreFactoryImpl(state_factory)

        name = name_factory.random()

        local_store = local_store_factory.open_or_create(TreeNodeImpl.empty())
        try:
            local_store.create(name, state_factory.random())

            err_ctx = self.assertRaises(RepoAlreadyExistsException)
            with (err_ctx):
                local_store.create(name, state_factory.random())
        finally:
            local_store.purge()

    def test_open_repo_not_exists(self) -> None:
        name_factory = NameFactoryMock()
        state_factory = StateFactoryMock()
        local_store_factory = LocalStoreFactoryImpl(state_factory)

        name = name_factory.random()

        local_store = local_store_factory.open_or_create(TreeNodeImpl.empty())
        try:
            err_ctx = self.assertRaises(RepoNotExistsException)
            with (err_ctx):
                local_store.open(name)
        finally:
            local_store.purge()


class LocalRepoTest(unittest.TestCase):
    def test_fetch_after_create(self) -> None:
        name_factory = NameFactoryMock()
        state_factory = StateFactoryMock()
        local_store_factory = LocalStoreFactoryImpl(state_factory)

        name = name_factory.random()
        state = state_factory.random()

        local_store = local_store_factory.open_or_create(TreeNodeImpl.empty())
        try:
            local_repo = local_store.create(name, state)

            self.assertEquals(
                first=state,
                second=local_repo.fetch_initial_state()
            )

            err_ctx = self.assertRaises(FileNotFoundError)
            with (err_ctx):
                local_repo.fetch_actual_state()
        finally:
            local_store.purge()

    def test_fetch_after_push(self) -> None:
        name_factory = NameFactoryMock()
        state_factory = StateFactoryMock()
        state_factory_generator = UniqueValueGenerator(state_factory.random)
        local_store_factory = LocalStoreFactoryImpl(state_factory)

        name = name_factory.random()
        initial_state = state_factory_generator.next()
        actual_state = state_factory_generator.next()

        local_store = local_store_factory.open_or_create(TreeNodeImpl.empty())
        try:
            local_repo = local_store.create(name, initial_state)

            local_repo.push(actual_state)

            self.assertEquals(
                first=initial_state,
                second=local_repo.fetch_initial_state()
            )

            self.assertEquals(
                first=actual_state,
                second=local_repo.fetch_actual_state()
            )
        finally:
            local_store.purge()
