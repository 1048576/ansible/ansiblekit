from typing import Dict
from typing import Optional

from ansible.plugins.action import ActionBase
from pygentree.node.abstract import TreeNode
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.cmd.pre.impl import PreCmdImpl
from ansiblekit.cookbook.name.abstract import NameFactory
from ansiblekit.cookbook.name.impl import NameFactoryImpl
from ansiblekit.cookbook.repo.local.abstract import LocalStoreFactory
from ansiblekit.cookbook.repo.local.impl import LocalStoreFactoryImpl
from ansiblekit.cookbook.repo.remote.abstract import RemoteStoreFactory
from ansiblekit.cookbook.state.abstract import StateFactory
from ansiblekit.cookbook.state.file.impl import StateFileFactoryImpl
from ansiblekit.cookbook.state.impl import StateFactoryImpl
from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.facts import Facts


class AnsibleActionResultImpl(ActionResult):
    pass


class AnsibleActionResultFactoryImpl(ActionResultFactory):
    def success(
        self,
        changed: bool,
        facts: Facts
    ) -> ActionResult:
        return AnsibleActionResultImpl()


class UnixAnsibleActionImpl(ActionBase):
    def run(
        self,
        tmp: None = None,
        task_vars: Optional[Dict[str, object]] = None
    ) -> Dict[str, object]:
        args = TreeNodeImpl.create(self._task.args)
        facts = TreeNodeImpl.create(task_vars)

        self._do_run(args, facts)

        return {}

    def _do_run(self, args: TreeNode, facts: TreeNode) -> ActionResult: ...

    def _state_factory(self) -> StateFactory:
        return StateFactoryImpl(StateFileFactoryImpl())

    def _local_store_factory(self) -> LocalStoreFactory:
        return LocalStoreFactoryImpl(
            state_factory=self._state_factory()
        )

    def _result_factory(self) -> ActionResultFactory:
        return AnsibleActionResultFactoryImpl()

    def _name_factory(self) -> NameFactory:
        return NameFactoryImpl()

    def _pre_cmd(self) -> PreCmd:
        return PreCmdImpl(
            local_store_factory=self._local_store_factory(),
            name_factory=self._name_factory()
        )

    def _remote_store_factory(self) -> RemoteStoreFactory:
        return RemoteStoreFactory()
