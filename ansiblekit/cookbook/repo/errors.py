from pygenerr.expected import ExpectedException

from ansiblekit.cookbook.name.abstract import Name


class RepoNotExistsException(ExpectedException):
    def __init__(self, name: Name) -> None:
        super().__init__(
            msg_template="Repo not exists [%s]",
            msg_vars=[name.uri()]
        )


class RepoAlreadyExistsException(ExpectedException):
    def __init__(self, name: Name) -> None:
        super().__init__(
            msg_template="Repo already exists [%s]",
            msg_vars=[name.uri()]
        )
