from pygentree.node.abstract import TreeNode

from ansiblekit.ansible.action import Action
from ansiblekit.ansible.unix.action import AnsibleActionUnix
from ansiblekit.ansible.unix.host import UnixHost


class ActionModule(AnsibleActionUnix):
    def create_action(self, args: TreeNode, host: UnixHost) -> Action:
        return Action()
