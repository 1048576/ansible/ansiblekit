from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.cmd.pre.abstract import PreCmdResult
from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.repo.local.abstract import LocalRepo
from ansiblekit.cookbook.state.abstract import State
from tests.ansiblekit.cookbook.repo.local.mock import LocalStoreFactoryMock


class PreCmdMock(PreCmd):
    _local_repo: LocalRepo
    _name: Name

    def __init__(
        self,
        name: Name,
        initial_state: State,
        actual_state: State
    ) -> None:
        local_store_factory = LocalStoreFactoryMock()
        local_store = local_store_factory.create()

        self._local_repo = local_store.create(name, initial_state)
        self._name = name

        self._local_repo.push(actual_state)

    def run(self, facts: TreeNode) -> PreCmdResult:
        return PreCmdResult(
            local_repo=self._local_repo,
            name=self._name
        )
