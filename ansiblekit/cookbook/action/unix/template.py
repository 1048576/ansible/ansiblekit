from __future__ import annotations

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.action.unix.deploy import UnixDeployAction
from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.cmd.unix.deploy.abstract import UnixDeployCmd
from ansiblekit.cookbook.unix.host import UnixHost
from ansiblekit.core.action.abstract import Action
from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.cmd.unix.stat.abstract import UnixStatCmd


class UnixTemplateDeployCmd(UnixDeployCmd):
    _src: str
    _host: UnixHost

    def __init__(self, src: str, host: UnixHost) -> None:
        self._src = src
        self._host = host

    def run(self, path: str, owner: str, group: str, mode: str) -> None:
        return self._host.template(self._src, path, owner, group, mode)


class UnixTemplateAction(Action):
    @staticmethod
    def create(
        args: TreeNode,
        host: UnixHost,
        pre_cmd: PreCmd,
        stat_cmd: UnixStatCmd,
        action_result_factory: ActionResultFactory
    ) -> Action:
        with (args):
            src = args.fetch_text("src")
            dest = args.fetch_text("dest")
            owner = args.fetch_text("owner")
            group = args.fetch_text("group")
            mode = args.fetch_text("mode")

        return UnixTemplateAction(
            src=src,
            dest=dest,
            owner=owner,
            group=group,
            mode=mode,
            host=host,
            pre_cmd=pre_cmd,
            stat_cmd=stat_cmd,
            action_result_factory=action_result_factory
        )

    _deploy_action: UnixDeployAction

    def __init__(
        self,
        src: str,
        dest: str,
        owner: str,
        group: str,
        mode: str,
        host: UnixHost,
        pre_cmd: PreCmd,
        stat_cmd: UnixStatCmd,
        action_result_factory: ActionResultFactory
    ) -> None:
        self._deploy_action = UnixDeployAction(
            path=dest,
            owner=owner,
            group=group,
            mode=mode,
            pre_cmd=pre_cmd,
            deploy_cmd=UnixTemplateDeployCmd(src, host),
            stat_cmd=stat_cmd,
            action_result_factory=action_result_factory
        )

    def run(self, facts: TreeNode) -> ActionResult:
        return self._deploy_action.run(facts)
