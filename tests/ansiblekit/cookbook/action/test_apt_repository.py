import unittest

import pygentreetest.node

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.action.unix.apt_repository import AptRepositoryAction
from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.unix.host import UnixHost
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.cmd.unix.stat.abstract import UnixStatCmd


class AptRepositoryActionTest(unittest.TestCase):
    def test_create_unsupported_args(self) -> None:
        def _callback(args: TreeNode) -> None:
            AptRepositoryAction.create(
                args=args,
                host=UnixHost(),
                pre_cmd=PreCmd(),
                stat_cmd=UnixStatCmd(),
                action_result_factory=ActionResultFactory()
            )

        pygentreetest.node.test_unsupported_keys(
            test_case=self,
            nodes={
                "repo": "",
                "filename": ""
            },
            callback=_callback
        )


if (__name__ == "__main__"):
    unittest.main()
