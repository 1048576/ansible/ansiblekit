from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.cmd.pre.abstract import CookbookNotOpenException
from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.cmd.pre.abstract import PreCmdResult
from ansiblekit.cookbook.name.abstract import CookbookNotSpecifiedException
from ansiblekit.cookbook.name.abstract import NameFactory
from ansiblekit.cookbook.repo.errors import RepoNotExistsException
from ansiblekit.cookbook.repo.local.abstract import LocalStoreFactory


class PreCmdImpl(PreCmd):
    _local_store_factory: LocalStoreFactory
    _name_factory: NameFactory

    def __init__(
        self,
        local_store_factory: LocalStoreFactory,
        name_factory: NameFactory
    ) -> None:
        self._local_store_factory = local_store_factory
        self._name_factory = name_factory

    def run(self, facts: TreeNode) -> PreCmdResult:
        try:
            name = self._name_factory.load(facts)
        except CookbookNotSpecifiedException:
            raise CookbookNotOpenException()

        local_store = self._local_store_factory.open_or_create(facts)

        try:
            local_repo = local_store.open(name)
        except RepoNotExistsException:
            raise CookbookNotOpenException()

        return PreCmdResult(
            local_repo=local_repo,
            name=name
        )
