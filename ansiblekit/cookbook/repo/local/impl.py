from __future__ import annotations

import json
import os
import os.path
import shutil

from pygenpath.resolver.abstract import PathResolver
from pygenpath.resolver.impl import PathResolverImpl
from pygentree.node.abstract import TreeNode
from pygentree.node.abstract import TreeNodeNotFoundException
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.repo.errors import RepoAlreadyExistsException
from ansiblekit.cookbook.repo.errors import RepoNotExistsException
from ansiblekit.cookbook.repo.local.abstract import LocalRepo
from ansiblekit.cookbook.repo.local.abstract import LocalStore
from ansiblekit.cookbook.repo.local.abstract import LocalStoreFactory
from ansiblekit.cookbook.state.abstract import State
from ansiblekit.cookbook.state.abstract import StateFactory
from ansiblekit.core.facts import Facts


class LocalStoreFactoryImpl(LocalStoreFactory):
    _state_factory: StateFactory

    def __init__(self, state_factory: StateFactory) -> None:
        self._state_factory = state_factory

    def open_or_create(self, facts: TreeNode) -> LocalStore:
        return LocalStoreImpl.open_or_create(
            state_factory=self._state_factory,
            facts=facts
        )


class LocalStoreImpl(LocalStore):
    KEY_WORKDIR: str = "_ansiblekit_cookbook_local_store_workdir"

    @staticmethod
    def open_or_create(
        state_factory: StateFactory,
        facts: TreeNode
    ) -> LocalStore:
        try:
            workdir = facts.fetch_text(LocalStoreImpl.KEY_WORKDIR)

            return LocalStoreImpl(
                state_factory=state_factory,
                workdir=PathResolverImpl.create(workdir)
            )
        except TreeNodeNotFoundException:
            return LocalStoreImpl(state_factory, PathResolverImpl.temp())

    _state_factory: StateFactory
    _workdir: PathResolver

    def __init__(
        self,
        state_factory: StateFactory,
        workdir: PathResolver
    ) -> None:
        self._state_factory = state_factory
        self._workdir = workdir

    def facts(self) -> Facts:
        return {
            LocalStoreImpl.KEY_WORKDIR: self._workdir.pwd()
        }

    def purge(self) -> None:
        shutil.rmtree(self._workdir.pwd())

    def create(self, name: Name, state: State) -> LocalRepo:
        workdir = self._workdir.cd("./%s/" % (name.uri()))

        try:
            os.mkdir(workdir.pwd())
        except FileExistsError:
            raise RepoAlreadyExistsException(name)

        return LocalRepoImpl.create(
            state_factory=self._state_factory,
            workdir=workdir,
            state=state
        )

    def open(self, name: Name) -> LocalRepo:
        workdir = self._workdir.cd("./%s/" % (name.uri()))

        if (not os.path.exists(workdir.pwd())):
            raise RepoNotExistsException(name)
        else:
            return LocalRepoImpl(
                state_factory=self._state_factory,
                workdir=workdir
            )


class LocalRepoImpl(LocalRepo):
    @staticmethod
    def create(
        state_factory: StateFactory,
        workdir: PathResolver,
        state: State
    ) -> LocalRepo:
        local_repo = LocalRepoImpl(
            state_factory=state_factory,
            workdir=workdir
        )

        local_repo._push(local_repo._initial_file_path, state)

        return local_repo

    _state_factory: StateFactory
    _initial_file_path: str
    _actual_file_path: str

    def __init__(
        self,
        state_factory: StateFactory,
        workdir: PathResolver
    ) -> None:
        self._state_factory = state_factory
        self._initial_file_path = workdir.resolve("./initial.json")
        self._actual_file_path = workdir.resolve("./actual.json")

    def fetch_initial_state(self) -> State:
        return self._fetch(self._initial_file_path)

    def fetch_actual_state(self) -> State:
        return self._fetch(self._actual_file_path)

    def push(self, state: State) -> None:
        return self._push(self._actual_file_path, state)

    def _fetch(self, path: str) -> State:
        f = open(path, "r")
        with (f):
            content = json.loads(f.read())

        facts = TreeNodeImpl.create(content)

        return self._state_factory.load(facts)

    def _push(self, path: str, state: State) -> None:
        f = open(path, "w")
        with (f):
            f.write(json.dumps(state.facts()) + "\n")
