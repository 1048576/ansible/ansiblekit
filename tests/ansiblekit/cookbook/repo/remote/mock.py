from typing import Dict

from pygentest.generator import UniqueTextGenerator
from pygentree.node.abstract import TreeNode
from pygentree.node.abstract import TreeNodeNotFoundException
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.repo.errors import RepoAlreadyExistsException
from ansiblekit.cookbook.repo.errors import RepoNotExistsException
from ansiblekit.cookbook.repo.remote.abstract import RemoteRepo
from ansiblekit.cookbook.repo.remote.abstract import RemoteStore
from ansiblekit.cookbook.repo.remote.abstract import RemoteStoreFactory
from ansiblekit.cookbook.state.abstract import State
from ansiblekit.cookbook.state.abstract import StateFactory
from ansiblekit.core.facts import Facts
from tests.ansiblekit.cookbook.state.mock import StateFactoryMock


class RemoteStoreFactoryMock(RemoteStoreFactory):
    _stores: Dict[str, RemoteStore]
    _uri_generator: UniqueTextGenerator
    _state_factory: StateFactory

    def __init__(
        self,
        state_factory: StateFactory = StateFactoryMock()
    ) -> None:
        self._stores = {}
        self._uri_generator = UniqueTextGenerator()
        self._state_factory = state_factory

    def open_or_create(self, facts: TreeNode) -> RemoteStore:
        return RemoteStoreMock.open_or_create(
            stores=self._stores,
            uri_generator=self._uri_generator,
            state_factory=self._state_factory,
            facts=facts
        )

    def create(self) -> RemoteStore:
        return self.open_or_create(TreeNodeImpl.empty())


class RemoteStoreMock(RemoteStore):
    KEY_URI = "_remote_repo_store_mock_uri"

    @staticmethod
    def open_or_create(
        stores: Dict[str, RemoteStore],
        uri_generator: UniqueTextGenerator,
        state_factory: StateFactory,
        facts: TreeNode
    ) -> RemoteStore:
        try:
            uri = facts.fetch_text(RemoteStoreMock.KEY_URI)

            return stores[uri]
        except TreeNodeNotFoundException:
            uri = uri_generator.next()
            store = RemoteStoreMock(state_factory, uri)

            stores[uri] = store

            return store

    _repos: Dict[str, RemoteRepo]
    _state_factory: StateFactory
    _uri: str

    def __init__(self, state_factory: StateFactory, uri: str) -> None:
        self._repos = {}
        self._state_factory = state_factory
        self._uri = uri

    def facts(self) -> Facts:
        return {
            RemoteStoreMock.KEY_URI: self._uri
        }

    def create(self, name: Name) -> RemoteRepo:
        if (name.uri() in self._repos):
            raise RepoAlreadyExistsException(name)

        repo = RemoteRepoMock(self._state_factory.create())

        self._repos[name.uri()] = repo

        return repo

    def open(self, name: Name) -> RemoteRepo:
        try:
            return self._repos[name.uri()]
        except KeyError:
            raise RepoNotExistsException(name)


class RemoteRepoMock(RemoteRepo):
    _state: State

    def __init__(self, state: State) -> None:
        self._state = state

    def fetch(self) -> State:
        return self._state

    def push(self, state: State) -> bool:
        changed = (self._state != state)

        self._state = state

        return changed
