import unittest
import uuid

from typing import Callable

import pygentest.not_eq
import pygentest.random

from pygenerr.expected import ExpectedException
from pygentest.generator import UniqueTextGenerator
from pygentree.node.abstract import TreeNode
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.cookbook.name.abstract import CookbookNotSpecifiedException
from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.name.abstract import NameFactory
from ansiblekit.cookbook.name.impl import NameFactoryImpl
from ansiblekit.cookbook.name.impl import NameImpl


class NameTestEq(unittest.TestCase):
    def test_eq(self) -> None:
        uri = pygentest.random.gen_text()

        lhs = NameImpl(uri)
        rhs = NameImpl(uri)

        self.assertEqual(
            first=lhs,
            second=rhs
        )

        self.assertEqual(
            first=lhs.uri(),
            second=rhs.uri()
        )

    def test_not_eq(self) -> None:
        uri_generator = UniqueTextGenerator()

        pygentest.not_eq.test(
            test_case=self,
            option="uri",
            lhs=NameImpl(uri_generator.next()),
            rhs=NameImpl(uri_generator.next())
        )


class NameFactoryTestInvalidCookbookName(unittest.TestCase):
    def test_create_invalid_name(self) -> None:
        def _callback(name_factory: NameFactory, uri: str) -> Name:
            return name_factory.create(uri)

        self._test(_callback)

    def test_load_invalid_name(self) -> None:
        def _callback(name_factory: NameFactory, uri: str) -> Name:
            invalid_name = NameImpl(uri)
            facts = TreeNodeImpl.create(invalid_name.facts())

            return name_factory.load(facts)

        self._test(_callback)

    def _test(self, callback: Callable[[NameFactory, str], Name]) -> None:
        name_factory = NameFactoryImpl()
        uri = pygentest.random.gen_text()

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            callback(name_factory, uri)

        self.assertEqual(
            first="Invalid cookbook name [%s]" % (uri),
            second=str(err_ctx.exception)
        )


class NameFactoryTestCookbookNotSpecified(unittest.TestCase):
    def test_load_cookbook_not_specified_case1(self) -> None:
        def _callback(name_factory: NameFactory) -> TreeNode:
            return TreeNodeImpl.empty()

        self._test(_callback)

    def test_load_cookbook_not_specified_case2(self) -> None:
        def _callback(name_factory: NameFactory) -> TreeNode:
            return TreeNodeImpl.create(name_factory.empty().facts())

        self._test(_callback)

    def _test(
        self,
        callback: Callable[[NameFactory], TreeNode]
    ) -> None:
        name_factory = NameFactoryImpl()
        facts = callback(name_factory)

        err_ctx = self.assertRaises(CookbookNotSpecifiedException)
        with (err_ctx):
            name_factory.load(facts)

        self.assertEqual(
            first="Cookbook not specified",
            second=str(err_ctx.exception)
        )


class NameFactoryTest(unittest.TestCase):
    def test_load(self) -> None:
        name_factory = NameFactoryImpl()
        uri = "%s.%s" % (pygentest.random.gen_text(), uuid.uuid1())

        expected = name_factory.create(uri)
        facts = TreeNodeImpl.create(expected.facts())
        actual = name_factory.load(facts)

        self.assertEqual(
            first=expected,
            second=actual
        )


if (__name__ == "__main__"):
    unittest.main()
