from pygentree.node.abstract import TreeNode
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.facts import Facts


class ActionResultMock(ActionResult):
    _changed: bool
    _facts: Facts

    def __init__(self, changed: bool, facts: Facts) -> None:
        self._changed = changed
        self._facts = facts

    def changed(self) -> bool:
        return self._changed

    def facts(self) -> TreeNode:
        return TreeNodeImpl.create(self._facts)


class ActionResultFactoryMock(ActionResultFactory):
    def success(self, changed: bool, facts: Facts) -> ActionResult:
        return ActionResultMock(changed, facts)
