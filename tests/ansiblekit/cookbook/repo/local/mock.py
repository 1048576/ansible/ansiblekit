from __future__ import annotations

from typing import Dict

from pygentest.generator import UniqueTextGenerator
from pygentree.node.abstract import TreeNode
from pygentree.node.abstract import TreeNodeNotFoundException
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.repo.errors import RepoAlreadyExistsException
from ansiblekit.cookbook.repo.errors import RepoNotExistsException
from ansiblekit.cookbook.repo.local.abstract import LocalRepo
from ansiblekit.cookbook.repo.local.abstract import LocalStore
from ansiblekit.cookbook.repo.local.abstract import LocalStoreFactory
from ansiblekit.cookbook.state.abstract import State
from ansiblekit.cookbook.state.abstract import StateFactory
from ansiblekit.core.facts import Facts
from tests.ansiblekit.cookbook.state.mock import StateFactoryMock


class LocalStoreFactoryMock(LocalStoreFactory):
    _stores: Dict[str, LocalStore]
    _uri_generator: UniqueTextGenerator
    _state_factory: StateFactory

    def __init__(
        self,
        state_factory: StateFactory = StateFactoryMock()
    ) -> None:
        self._stores = {}
        self._uri_generator = UniqueTextGenerator()
        self._state_factory = state_factory

    def open_or_create(self, facts: TreeNode) -> LocalStore:
        return LocalStoreMock.open_or_create(
            stores=self._stores,
            uri_generator=self._uri_generator,
            state_factory=self._state_factory,
            facts=facts
        )

    def create(self) -> LocalStore:
        return self.open_or_create(TreeNodeImpl.empty())


class LocalStoreMock(LocalStore):
    KEY_URI = "_local_store_mock_uri"

    @staticmethod
    def open_or_create(
        stores: Dict[str, LocalStore],
        uri_generator: UniqueTextGenerator,
        state_factory: StateFactory,
        facts: TreeNode
    ) -> LocalStore:
        try:
            uri = facts.fetch_text(LocalStoreMock.KEY_URI)

            return stores[uri]
        except TreeNodeNotFoundException:
            uri = uri_generator.next()
            store = LocalStoreMock(state_factory, uri)

            stores[uri] = store

            return store

    _repos: Dict[str, LocalRepo]
    _state_factory: StateFactory
    _uri: str

    def __init__(self, state_factory: StateFactory, uri: str) -> None:
        self._repos = {}
        self._state_factory = state_factory
        self._uri = uri

    def facts(self) -> Facts:
        return {
            LocalStoreMock.KEY_URI: self._uri
        }

    def create(self, name: Name, state: State) -> LocalRepo:
        if (name.uri() in self._repos):
            raise RepoAlreadyExistsException(name)

        repo = LocalRepoMock(state, self._state_factory.create())

        self._repos[name.uri()] = repo

        return repo

    def open(self, name: Name) -> LocalRepo:
        try:
            return self._repos[name.uri()]
        except KeyError:
            raise RepoNotExistsException(name)


class LocalRepoMock(LocalRepo):
    _initial_state: State
    _actual_state: State

    def __init__(self, initial_state: State, actual_state: State) -> None:
        self._initial_state = initial_state
        self._actual_state = actual_state

    def fetch_initial_state(self) -> State:
        return self._initial_state

    def fetch_actual_state(self) -> State:
        return self._actual_state

    def push(self, state: State) -> None:
        self._actual_state = state
