from __future__ import annotations

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.state.abstract import State
from ansiblekit.core.facts import Facts


class LocalStoreFactory:
    def open_or_create(self, facts: TreeNode) -> LocalStore: ...


class LocalStore:
    def facts(self) -> Facts: ...

    def purge(self) -> None: ...

    def create(self, name: Name, state: State) -> LocalRepo: ...

    def open(self, name: Name) -> LocalRepo: ...


class LocalRepo:
    def fetch_initial_state(self) -> State: ...

    def fetch_actual_state(self) -> State: ...

    def push(self, state: State) -> None: ...
