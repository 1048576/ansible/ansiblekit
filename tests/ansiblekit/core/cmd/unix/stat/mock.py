from typing import Dict

from ansiblekit.core.cmd.unix.stat.abstract import UnixStatCmd
from ansiblekit.core.file.unix.abstract import UnixFile


class UnixStatCmdMock(UnixStatCmd):
    _files: Dict[str, UnixFile]

    def __init__(
        self,
        files: Dict[str, UnixFile]
    ) -> None:
        self._files = files

    def run(self, path: str) -> UnixFile:
        return self._files[path]
