from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.name.abstract import NameFactory
from ansiblekit.cookbook.repo.remote.abstract import RemoteStoreFactory
from ansiblekit.core.action.abstract import Action
from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.core.action.abstract import ActionResultFactory


class CloseAction(Action):
    @staticmethod
    def create(
        args: TreeNode,
        remote_store_factory: RemoteStoreFactory,
        pre_cmd: PreCmd,
        name_factory: NameFactory,
        action_result_factory: ActionResultFactory
    ) -> Action:
        with (args):
            pass

        return CloseAction(
            remote_store_factory=remote_store_factory,
            pre_cmd=pre_cmd,
            name_factory=name_factory,
            action_result_factory=action_result_factory
        )

    _remote_store_factory: RemoteStoreFactory
    _pre_cmd: PreCmd
    _name_factory: NameFactory
    _action_result_factory: ActionResultFactory

    def __init__(
        self,
        remote_store_factory: RemoteStoreFactory,
        pre_cmd: PreCmd,
        name_factory: NameFactory,
        action_result_factory: ActionResultFactory
    ) -> None:
        self._remote_store_factory = remote_store_factory
        self._pre_cmd = pre_cmd
        self._name_factory = name_factory
        self._action_result_factory = action_result_factory

    def run(self, facts: TreeNode) -> ActionResult:
        pre_cmd_result = self._pre_cmd.run(facts)

        remote_store = self._remote_store_factory.open_or_create(facts)
        remote_repo = remote_store.open(pre_cmd_result.name())

        actual_state = pre_cmd_result.local_repo().fetch_actual_state()

        changed = remote_repo.push(actual_state)

        return self._action_result_factory.success(
            changed=changed,
            facts=self._name_factory.empty().facts()
        )
