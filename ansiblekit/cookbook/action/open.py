from pygenerr.expected import ExpectedException
from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.name.abstract import CookbookNotSpecifiedException
from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.name.abstract import NameFactory
from ansiblekit.cookbook.repo.errors import RepoAlreadyExistsException
from ansiblekit.cookbook.repo.errors import RepoNotExistsException
from ansiblekit.cookbook.repo.local.abstract import LocalStoreFactory
from ansiblekit.cookbook.repo.remote.abstract import RemoteStoreFactory
from ansiblekit.core.action.abstract import Action
from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.core.action.abstract import ActionResultFactory


class OtherCookbookAlreadyOpenedException(ExpectedException):
    def __init__(self, name: Name) -> None:
        super().__init__(
            msg_template="Other cookbook already opened [%s]",
            msg_vars=[name.uri()]
        )


class NotUseTheSameCookbookTwiceException(ExpectedException):
    def __init__(self, name: Name) -> None:
        super().__init__(
            msg_template="Not use the same cookbook twice [%s]",
            msg_vars=[name.uri()]
        )


class OpenAction(Action):
    @staticmethod
    def create(
        args: TreeNode,
        local_store_factory: LocalStoreFactory,
        remote_store_factory: RemoteStoreFactory,
        name_factory: NameFactory,
        action_result_factory: ActionResultFactory
    ) -> Action:
        with (args):
            uri = args.fetch_text("uri")

        return OpenAction(
            uri=uri,
            local_store_factory=local_store_factory,
            remote_store_factory=remote_store_factory,
            name_factory=name_factory,
            action_result_factory=action_result_factory
        )

    _uri: str
    _local_store_factory: LocalStoreFactory
    _remote_store_factory: RemoteStoreFactory
    _name_factory: NameFactory
    _action_result_factory: ActionResultFactory

    def __init__(
        self,
        uri: str,
        local_store_factory: LocalStoreFactory,
        remote_store_factory: RemoteStoreFactory,
        name_factory: NameFactory,
        action_result_factory: ActionResultFactory
    ) -> None:
        self._uri = uri
        self._local_store_factory = local_store_factory
        self._remote_store_factory = remote_store_factory
        self._name_factory = name_factory
        self._action_result_factory = action_result_factory

    def run(self, facts: TreeNode) -> ActionResult:
        try:
            name = self._name_factory.load(facts)

            raise OtherCookbookAlreadyOpenedException(name)
        except CookbookNotSpecifiedException:
            name = self._name_factory.create(self._uri)

        remote_store = self._remote_store_factory.open_or_create(facts)
        try:
            remote_repo = remote_store.open(name)
            changed = False
        except RepoNotExistsException:
            remote_repo = remote_store.create(name)
            changed = True

        state = remote_repo.fetch()

        local_store = self._local_store_factory.open_or_create(facts)
        try:
            local_store.create(name, state)
        except RepoAlreadyExistsException:
            raise NotUseTheSameCookbookTwiceException(name)

        return self._action_result_factory.success(
            changed=changed,
            facts={
                **remote_store.facts(),
                **local_store.facts(),
                **name.facts()
            }
        )
