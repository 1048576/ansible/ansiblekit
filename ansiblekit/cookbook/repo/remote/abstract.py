from __future__ import annotations

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.state.abstract import State
from ansiblekit.core.facts import Facts


class RemoteStoreFactory:
    def open_or_create(self, facts: TreeNode) -> RemoteStore: ...


class RemoteStore:
    def facts(self) -> Facts: ...

    def create(self, name: Name) -> RemoteRepo: ...

    def open(self, name: Name) -> RemoteRepo: ...


class RemoteRepo:
    def fetch(self) -> State: ...

    def push(self, state: State) -> bool: ...
