import base64

from typing import Dict

from pygentree.node.impl import TreeNodeImpl

from ansiblekit.ansible.result import ActionResult


class SlurpResult(ActionResult):
    content: bytes

    def __init__(self, fields: Dict[str, object]) -> None:
        super().__init__(fields)

        tree = TreeNodeImpl.create(fields)

        encoding_actual = tree.fetch_text("encoding")
        encoding_expected = "base64"
        if (encoding_expected != encoding_actual):
            err_msg_template = "[%s] insted of [%s] encoding"
            err_msg_vars = (encoding_actual, encoding_expected)

            raise Exception(err_msg_template % err_msg_vars)

        encoding_content = tree.fetch_text("content")
        self.content = base64.decodebytes(encoding_content.encode())
