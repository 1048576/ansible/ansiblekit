import unittest

import pygentreetest.node

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.action.unix.stat import UnixStatAction
from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.core.action.abstract import Action
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.cmd.unix.stat.abstract import UnixStatCmd


class UnixStatActionTest(unittest.TestCase):
    def test_create_unsupported_args(self) -> None:
        def _callback(args: TreeNode) -> Action:
            return UnixStatAction.create(
                args=args,
                pre_cmd=PreCmd(),
                stat_cmd=UnixStatCmd(),
                action_result_factory=ActionResultFactory()
            )

        pygentreetest.node.test_unsupported_keys(
            test_case=self,
            nodes={
                "path": "",
                "owner": "",
                "group": "",
                "mode": ""
            },
            callback=_callback
        )


if (__name__ == "__main__"):
    unittest.main()
