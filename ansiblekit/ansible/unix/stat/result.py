from __future__ import annotations

from typing import Dict

from pygentree.node.impl import TreeNodeImpl

from ansiblekit.ansible.result import ActionResult


class UnixStatResult(ActionResult):
    @staticmethod
    def create(fields: Dict[str, object]) -> UnixStatResult:
        root_node = TreeNodeImpl.create(fields)
        stat_node = root_node.fetch_node("stat")

        return UnixStatResult(
            fields=fields,
            path=stat_node.fetch_text("path"),
            checksum=stat_node.fetch_text("checksum"),
            owner=stat_node.fetch_text("pw_name"),
            group=stat_node.fetch_text("gr_name"),
            mode=stat_node.fetch_text("mode"),
            ctime=str(stat_node.fetch_float("ctime"))
        )

    path: str
    checksum: str
    owner: str
    group: str
    mode: str
    ctime: str

    def __init__(
        self,
        fields: Dict[str, object],
        path: str,
        checksum: str,
        owner: str,
        group: str,
        mode: str,
        ctime: str
    ) -> None:
        super().__init__(fields)

        self.path = path
        self.checksum = checksum
        self.owner = owner
        self.group = group
        self.mode = mode
        self.ctime = ctime
