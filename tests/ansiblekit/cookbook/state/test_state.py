import unittest

from typing import Callable
from typing import Dict
from typing import Generic
from typing import Protocol
from typing import TypeVar

import pygentest.not_eq
import pygentest.random

from pygentest.generator import UniqueTextGenerator
from pygentest.generator import UniqueValueGenerator

from ansiblekit.cookbook.state.abstract import State
from ansiblekit.cookbook.state.abstract import UpdateStateResult
from ansiblekit.cookbook.state.file.abstract import StateFile
from ansiblekit.cookbook.state.file.abstract import StateFileFactory
from ansiblekit.cookbook.state.impl import StateImpl
from tests.ansiblekit.cookbook.state.file.mock import StateFileFactoryMock

_T = TypeVar("_T", contravariant=True)


class _StateTestUpdateCallback(Generic[_T], Protocol):
    def __call__(
        self,
        actual_state: State,
        key: str,
        value: _T,
        initial_state: State
    ) -> UpdateStateResult: ...


class StateTestUpdate:
    def run_test_update(
        self,
        test_case: unittest.TestCase,
        state_factory: Callable[[Dict[str, _T]], State],
        value_factory: Callable[[], _T],
        callback: _StateTestUpdateCallback[_T],
        accessor: Callable[[State], Dict[str, _T]]
    ) -> None:
        key_generator = UniqueTextGenerator()
        value_generator = UniqueValueGenerator(value_factory)

        state = state_factory({})
        expected: Dict[str, _T] = {}

        for _ in range(0, 2):
            key = key_generator.next()
            value = value_generator.next()
            expected[key] = value

            update_state_result = callback(state, key, value, state)
            state = update_state_result.state()

            actual = accessor(state)

            test_case.assertEqual(
                first=expected,
                second=actual
            )

    def run_test_update_modified(
        self,
        test_case: unittest.TestCase,
        state_factory: Callable[[Dict[str, _T]], State],
        value_factory: Callable[[], _T],
        callback: _StateTestUpdateCallback[_T]
    ) -> None:
        def _test(
            actual_state: State,
            initial_state: State
        ) -> None:
            update_state_result = callback(
                actual_state=actual_state,
                key=key,
                value=actual_value,
                initial_state=initial_state
            )

            test_case.assertTrue(
                expr=update_state_result.changed()
            )

        value_generator = UniqueValueGenerator(value_factory)
        key = pygentest.random.gen_text()

        actual_value = value_generator.next()
        initial_value = value_generator.next()

        _test(
            actual_state=state_factory({key: actual_value}),
            initial_state=state_factory({key: initial_value})
        )

        _test(
            actual_state=state_factory({}),
            initial_state=state_factory({key: initial_value})
        )

        _test(
            actual_state=state_factory({key: actual_value}),
            initial_state=state_factory({})),

        _test(
            actual_state=state_factory({}),
            initial_state=state_factory({})
        )

    def run_test_update_not_modified(
        self,
        test_case: unittest.TestCase,
        state_factory: Callable[[Dict[str, _T]], State],
        value_factory: Callable[[], _T],
        callback: _StateTestUpdateCallback[_T]
    ) -> None:
        def _test(
            actual_state: State,
            initial_state: State
        ) -> None:
            update_state_result = callback(
                actual_state=actual_state,
                key=key,
                value=actual_value,
                initial_state=initial_state
            )

            test_case.assertFalse(
                expr=update_state_result.changed()
            )

        value_generator = UniqueValueGenerator(value_factory)
        key = pygentest.random.gen_text()

        actual_value = value_generator.next()
        initial_value = value_generator.next()

        _test(
            actual_state=state_factory({key: initial_value}),
            initial_state=state_factory({key: actual_value})
        )

        _test(
            actual_state=state_factory({}),
            initial_state=state_factory({key: actual_value})
        )


class StateTestUpdateVar(unittest.TestCase, StateTestUpdate):
    @staticmethod
    def _state_factory(vars: Dict[str, str]) -> State:
        return StateImpl(
            state_file_factory=StateFileFactory(),
            vars=vars,
            files={}
        )

    @staticmethod
    def _callback(
        actual_state: State,
        key: str,
        value: str,
        initial_state: State
    ) -> UpdateStateResult:
        return actual_state.update_var(
            key=key,
            value=value,
            state=initial_state
        )

    def test_update_var(self) -> None:
        self.run_test_update(
            test_case=self,
            state_factory=StateTestUpdateVar._state_factory,
            value_factory=pygentest.random.gen_text,
            callback=StateTestUpdateVar._callback,
            accessor=lambda state: state.vars()
        )

    def test_update_var_modified(self) -> None:
        self.run_test_update_modified(
            test_case=self,
            state_factory=StateTestUpdateVar._state_factory,
            value_factory=pygentest.random.gen_text,
            callback=StateTestUpdateVar._callback
        )

    def test_update_var_not_modified(self) -> None:
        self.run_test_update_not_modified(
            test_case=self,
            state_factory=StateTestUpdateVar._state_factory,
            value_factory=pygentest.random.gen_text,
            callback=StateTestUpdateVar._callback
        )


class StateTestUpdateFile(unittest.TestCase, StateTestUpdate):
    @staticmethod
    def _state_factory(files: Dict[str, StateFile]) -> State:
        return StateImpl(
            state_file_factory=StateFileFactoryMock(),
            vars={},
            files=files
        )

    @staticmethod
    def _callback(
        actual_state: State,
        key: str,
        value: StateFile,
        initial_state: State
    ) -> UpdateStateResult:
        return actual_state.update_file(
            path=key,
            checksum=value.checksum(),
            ctime=value.ctime(),
            state=initial_state
        )

    def test_update_file(self) -> None:
        self.run_test_update(
            test_case=self,
            state_factory=StateTestUpdateFile._state_factory,
            value_factory=StateFileFactoryMock.random_checksum,
            callback=StateTestUpdateFile._callback,
            accessor=lambda state: state.files()
        )

    def test_update_file_modified_checksum(self) -> None:
        self.run_test_update_not_modified(
            test_case=self,
            state_factory=StateTestUpdateFile._state_factory,
            value_factory=StateFileFactoryMock.random_checksum,
            callback=StateTestUpdateFile._callback
        )

    def test_update_file_modified_ctime(self) -> None:
        self.run_test_update_not_modified(
            test_case=self,
            state_factory=StateTestUpdateFile._state_factory,
            value_factory=StateFileFactoryMock.random_ctime,
            callback=StateTestUpdateFile._callback
        )

    def test_update_file_not_modified(self) -> None:
        self.run_test_update_not_modified(
            test_case=self,
            state_factory=StateTestUpdateFile._state_factory,
            value_factory=StateFileFactoryMock.random,
            callback=StateTestUpdateFile._callback
        )


class StateTestEq(unittest.TestCase):
    def test_eq(self) -> None:
        key_generator = UniqueTextGenerator()

        state_file_factory = StateFileFactory()
        var_key = key_generator.next()
        var_value = pygentest.random.gen_text()
        file_key = key_generator.next()
        file_value = StateFile()
        vars = {
            var_key: var_value
        }
        files = {
            file_key: file_value
        }

        self.assertEqual(
            first=StateImpl(state_file_factory, vars, files),
            second=StateImpl(state_file_factory, vars, files)
        )


class StateTestNotEq(unittest.TestCase):
    def test_not_eq_vars(self) -> None:
        def _state_factory(vars: Dict[str, str]) -> State:
            return StateImpl(
                state_file_factory=StateFileFactory(),
                vars=vars,
                files={}
            )

        self._test(
            state_factory=_state_factory,
            value_factory=pygentest.random.gen_text
        )

    def test_not_eq_files(self) -> None:
        def _state_factory(
            files: Dict[str, StateFile]
        ) -> State:
            return StateImpl(
                state_file_factory=StateFileFactory(),
                vars={},
                files=files
            )

        self._test(
            state_factory=_state_factory,
            value_factory=StateFile
        )

    def _test(
        self,
        state_factory: Callable[[Dict[str, _T]], State],
        value_factory: Callable[[], _T]
    ) -> None:
        key_generator = UniqueTextGenerator()
        value_generator = UniqueValueGenerator(value_factory)

        k1 = key_generator.next()
        k2 = key_generator.next()
        v1 = value_generator.next()
        v2 = value_generator.next()

        lhs = state_factory({
            k1: v1
        })

        pygentest.not_eq.test(
            test_case=self,
            option="empty",
            lhs=lhs,
            rhs=state_factory({})
        )

        pygentest.not_eq.test(
            test_case=self,
            option="count",
            lhs=lhs,
            rhs=state_factory({
                k1: v1,
                k2: v2
            })
        )

        pygentest.not_eq.test(
            test_case=self,
            option="key",
            lhs=lhs,
            rhs=state_factory({
                k2: v1
            })
        )

        pygentest.not_eq.test(
            test_case=self,
            option="value",
            lhs=lhs,
            rhs=state_factory({
                k1: v2
            })
        )


if (__name__ == "__main__"):
    unittest.main()
