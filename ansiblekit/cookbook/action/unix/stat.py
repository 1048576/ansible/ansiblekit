from __future__ import annotations

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.action.unix.deploy import UnixDeployAction
from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.cmd.unix.deploy.abstract import UnixDeployCmd
from ansiblekit.core.action.abstract import Action
from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.cmd.unix.stat.abstract import UnixStatCmd


class UnixStatAction(Action):
    @staticmethod
    def create(
        args: TreeNode,
        pre_cmd: PreCmd,
        stat_cmd: UnixStatCmd,
        action_result_factory: ActionResultFactory
    ) -> Action:
        with (args):
            path = args.fetch_text("path")
            owner = args.fetch_text("owner")
            group = args.fetch_text("group")
            mode = args.fetch_text("mode")

        return UnixStatAction(
            path=path,
            owner=owner,
            group=group,
            mode=mode,
            pre_cmd=pre_cmd,
            stat_cmd=stat_cmd,
            action_result_factory=action_result_factory
        )

    _deploy_action: UnixDeployAction

    def __init__(
        self,
        path: str,
        owner: str,
        group: str,
        mode: str,
        pre_cmd: PreCmd,
        stat_cmd: UnixStatCmd,
        action_result_factory: ActionResultFactory
    ) -> None:
        self._deploy_action = UnixDeployAction(
            path=path,
            owner=owner,
            group=group,
            mode=mode,
            pre_cmd=pre_cmd,
            deploy_cmd=UnixDeployCmd(),
            stat_cmd=stat_cmd,
            action_result_factory=action_result_factory
        )

    def run(self, facts: TreeNode) -> ActionResult:
        return self._deploy_action.run(facts)
