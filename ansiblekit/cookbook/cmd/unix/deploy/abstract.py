class UnixDeployCmd:
    def run(
        self,
        path: str,
        owner: str,
        group: str,
        mode: str
    ) -> None: ...
