from ansiblekit.core.file.unix.abstract import UnixFile


class UnixStatCmd:
    def run(self, path: str) -> UnixFile: ...
