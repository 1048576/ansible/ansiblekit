import uuid

import pygentest.random

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.name.abstract import NameFactory
from ansiblekit.cookbook.name.impl import NameFactoryImpl


class NameFactoryMock(NameFactory):
    ORIGIN: NameFactory = NameFactoryImpl()

    @staticmethod
    def random() -> Name:
        return NameFactoryMock.ORIGIN.create(
            uri="%s.%s" % (pygentest.random.gen_text(), uuid.uuid1())
        )

    @staticmethod
    def empty() -> Name:
        return NameFactoryMock.ORIGIN.empty()

    @staticmethod
    def create(uri: str) -> Name:
        return NameFactoryMock.ORIGIN.create(uri)

    @staticmethod
    def load(facts: TreeNode) -> Name:
        return NameFactoryMock.ORIGIN.load(facts)
