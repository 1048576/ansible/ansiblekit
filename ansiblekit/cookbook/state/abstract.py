from __future__ import annotations

from typing import Dict

from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.state.file.abstract import StateFile


class UpdateStateResult:
    _changed: bool
    _state: State

    def __init__(self, changed: bool, state: State) -> None:
        self._changed = changed
        self._state = state

    def changed(self) -> bool:
        return self._changed

    def state(self) -> State:
        return self._state


class State:
    def facts(self) -> object: ...

    def vars(self) -> Dict[str, str]: ...

    def files(self) -> Dict[str, StateFile]: ...

    def update_var(
        self,
        key: str,
        value: str,
        state: State
    ) -> UpdateStateResult: ...

    def update_file(
        self,
        path: str,
        checksum: str,
        ctime: str,
        state: State
    ) -> UpdateStateResult: ...


class StateFactory:
    def create(self) -> State: ...

    def load(self, facts: TreeNode) -> State: ...
