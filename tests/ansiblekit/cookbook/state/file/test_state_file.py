import unittest

import pygentest.not_eq
import pygentreetest.node

from pygentest.generator import UniqueTextGenerator

from ansiblekit.cookbook.state.file.impl import StateFileFactoryImpl
from ansiblekit.cookbook.state.file.impl import StateFileImpl


class StateFileTestEq(unittest.TestCase):
    def test_eq(self) -> None:
        checksum_generator = UniqueTextGenerator()
        ctime_generator = UniqueTextGenerator()

        checksum = checksum_generator.next()
        ctime = ctime_generator.next()

        self.assertEqual(
            first=StateFileImpl(checksum, ctime),
            second=StateFileImpl(checksum, ctime)
        )

    def test_not_eq(self) -> None:
        checksum_generator = UniqueTextGenerator()
        ctime_generator = UniqueTextGenerator()

        checksum1 = checksum_generator.next()
        checksum2 = checksum_generator.next()
        ctime1 = ctime_generator.next()
        ctime2 = ctime_generator.next()

        lhs = StateFileImpl(checksum1, ctime1)

        pygentest.not_eq.test(
            test_case=self,
            option="checksum",
            lhs=lhs,
            rhs=StateFileImpl(checksum2, ctime1)
        )

        pygentest.not_eq.test(
            test_case=self,
            option="ctime",
            lhs=lhs,
            rhs=StateFileImpl(checksum1, ctime2)
        )


class StateFileFactoryTest(unittest.TestCase):
    def test_load_unsupported_args(self) -> None:
        factory = StateFileFactoryImpl()

        pygentreetest.node.test_unsupported_keys(
            test_case=self,
            nodes={
                "checksum": "",
                "ctime": ""
            },
            callback=factory.load
        )


if (__name__ == "__main__"):
    unittest.main()
