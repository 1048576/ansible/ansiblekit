import pygentest.random

from pygentest.generator import UniqueTextGenerator
from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.state.file.abstract import StateFile
from ansiblekit.cookbook.state.file.abstract import StateFileFactory
from ansiblekit.cookbook.state.file.impl import StateFileFactoryImpl


class StateFileFactoryMock(StateFileFactory):
    ORIGIN: StateFileFactory = StateFileFactoryImpl()

    @staticmethod
    def random() -> StateFile:
        text_generator = UniqueTextGenerator()

        return StateFileFactoryMock.ORIGIN.create(
            checksum=text_generator.next(),
            ctime=text_generator.next()
        )

    @staticmethod
    def random_checksum() -> StateFile:
        return StateFileFactoryMock.ORIGIN.create(
            checksum=pygentest.random.gen_text(),
            ctime=""
        )

    @staticmethod
    def random_ctime() -> StateFile:
        return StateFileFactoryMock.ORIGIN.create(
            checksum="",
            ctime=pygentest.random.gen_text()
        )

    def create(self, checksum: str, ctime: str) -> StateFile:
        return StateFileFactoryMock.ORIGIN.create(checksum, ctime)

    def load(self, facts: TreeNode) -> StateFile:
        return StateFileFactoryMock.ORIGIN.load(facts)
