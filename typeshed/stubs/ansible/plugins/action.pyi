from typing import Dict
from typing import Optional

from ansible.playbook.play_context import PlayContext
from ansible.playbook.task import Task
from ansible.plugins.connection import ConnectionBase
from ansible.template import Templar

class ActionBase:
    _task: Task
    _connection: ConnectionBase
    _templar: Templar
    _play_context: PlayContext

    def run(
        self,
        tmp: None = ...,
        task_vars: Optional[Dict[str, object]] = ...
    ) -> Dict[str, object]: ...
