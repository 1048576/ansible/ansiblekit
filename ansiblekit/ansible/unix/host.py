from typing import Optional

import ansiblekit.core.action.template.options as template_options

from ansiblekit.ansible.action import AnsibleExecutor
from ansiblekit.ansible.result import ActionException
from ansiblekit.ansible.result import ActionResult
from ansiblekit.ansible.result import FileNotFoundException
from ansiblekit.ansible.unix.stat.result import UnixStatResult
from ansiblekit.core.action.slurp.result import SlurpResult


class UnixHost():
    def slurp(self, src: str) -> SlurpResult: ...

    def mkdir(
        self,
        path: str,
        owner: str,
        group: str,
        mode: str
    ) -> ActionResult: ...

    def stat(self, path: str) -> UnixStatResult: ...

    def copy(
        self,
        content: Optional[str],
        dest: str,
        owner: str,
        group: str,
        mode: str
    ) -> ActionResult: ...

    def template(
        self,
        src: str,
        dest: str,
        owner: str,
        group: str,
        mode: str
    ) -> ActionResult: ...

    def apt_repository(
        self,
        repo: str,
        filename: str
    ) -> ActionResult: ...


class UnixHostImpl(UnixHost):
    executor: AnsibleExecutor

    def __init__(self, executor: AnsibleExecutor) -> None:
        self.executor = executor

    def slurp(self, src: str) -> SlurpResult:
        try:
            result = self.executor.execute_module(
                name="ansible.builtin.slurp",
                args={
                    "src": src
                }
            )

            return SlurpResult(result)
        except ActionException as e:
            expected_err_msg = "file not found: %s" % (src)
            if (e.result.msg() == expected_err_msg):
                raise FileNotFoundException(e.result)
            else:
                raise e

    def mkdir(
        self,
        path: str,
        owner: str,
        group: str,
        mode: str
    ) -> ActionResult:
        return self.executor.execute_module(
            name="ansible.builtin.file",
            args={
                "path": path,
                "state": "directory",
                "owner": owner,
                "group": group,
                "mode": mode
            }
        )

    def stat(self, path: str) -> UnixStatResult:
        result = self.executor.execute_module(
            name="ansible.builtin.stat",
            args={
                "path": path
            }
        )

        return UnixStatResult.create(result)

    def copy(
        self,
        content: Optional[str],
        dest: str,
        owner: str,
        group: str,
        mode: str
    ) -> ActionResult:
        return self.executor.execute_action(
            name="ansible.builtin.copy",
            args={
                "content": content,
                "dest": dest,
                "owner": owner,
                "group": group,
                "mode": mode
            }
        )

    def template(
        self,
        src: str,
        dest: str,
        owner: str,
        group: str,
        mode: str
    ) -> ActionResult:
        args = {
            "src": src,
            "dest": dest,
            "owner": owner,
            "group": group,
            "mode": mode
        }

        return self.executor.execute_action(
            name="ansible.builtin.template",
            args={
                **template_options.DEFAULT_TEMPLATE_OPTIONS,
                **args
            }
        )

    def apt_repository(
        self,
        repo: str,
        filename: str
    ) -> ActionResult:
        return self.executor.execute_module(
            name="ansible.builtin.apt_repository",
            args={
                "repo": repo,
                "state": "present",
                "filename": filename
            }
        )
