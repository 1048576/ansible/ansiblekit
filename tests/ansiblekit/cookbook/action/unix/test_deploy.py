import unittest

from typing import Callable
from typing import Dict

import pygentest.random

from pygenerr.expected import ExpectedException
from pygentest.generator import UniqueTextGenerator
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.cookbook.action.unix.deploy import UnixDeployAction
from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.cmd.unix.deploy.abstract import UnixDeployCmd
from ansiblekit.cookbook.state.file.abstract import StateFile
from ansiblekit.core.action.abstract import Action
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.cmd.unix.stat.abstract import UnixStatCmd
from ansiblekit.core.file.unix.abstract import UnixFile
from tests.ansiblekit.cookbook.action.mock import ActionResultFactoryMock
from tests.ansiblekit.cookbook.cmd.pre.mock import PreCmdMock
from tests.ansiblekit.cookbook.cmd.unix.deploy.mock import UnixDeployCmdMock
from tests.ansiblekit.cookbook.name.mock import NameFactoryMock
from tests.ansiblekit.cookbook.state.file.mock import StateFileFactoryMock
from tests.ansiblekit.cookbook.state.mock import StateFactoryMock
from tests.ansiblekit.core.cmd.unix.stat.mock import UnixStatCmdMock
from tests.ansiblekit.core.file.unix.mock import UnixFileMock


class DeployActionTestRun(unittest.TestCase):
    def test_run_not_changed(self) -> None:
        state_file_factory = StateFileFactoryMock()
        text_generator = UniqueTextGenerator()

        checksum = text_generator.next()
        ctime = text_generator.next()

        self._test(
            expected=state_file_factory.create(
                checksum=checksum,
                ctime=ctime
            ),
            actual=state_file_factory.create(
                checksum=checksum,
                ctime=ctime
            )
        )

    def test_run_changed_checksum(self) -> None:
        state_file_factory = StateFileFactoryMock()
        text_generator = UniqueTextGenerator()

        self._test(
            expected=state_file_factory.create(
                checksum=text_generator.next(),
                ctime=""
            ),
            actual=state_file_factory.create(
                checksum=text_generator.next(),
                ctime=""
            )
        )

    def test_run_changed_ctime(self) -> None:
        state_file_factory = StateFileFactoryMock()
        text_generator = UniqueTextGenerator()

        self._test(
            expected=state_file_factory.create(
                checksum="",
                ctime=text_generator.next()
            ),
            actual=state_file_factory.create(
                checksum="",
                ctime=text_generator.next()
            )
        )

    def _test(
        self,
        expected: StateFile,
        actual: StateFile
    ) -> None:
        state_factory = StateFactoryMock()
        name_factory = NameFactoryMock()
        action_result_factory = ActionResultFactoryMock()

        name = name_factory.random()
        path = pygentest.random.gen_text()

        source: Dict[str, StateFile] = {
            path: actual
        }
        target: Dict[str, UnixFile] = {}

        initial_state = state_factory.create_and_update_file(path, expected)
        actual_state = state_factory.random()

        action = UnixDeployAction(
            path=path,
            owner="",
            group="",
            mode="",
            pre_cmd=PreCmdMock(
                name=name,
                initial_state=initial_state,
                actual_state=actual_state
            ),
            deploy_cmd=UnixDeployCmdMock(
                source=source,
                target=target
            ),
            stat_cmd=UnixStatCmdMock(target),
            action_result_factory=action_result_factory
        )

        result = action.run(TreeNodeImpl.create(name.facts()))

        self.assertEqual(
            first=(expected != actual),
            second=result.changed()
        )

        self.assertEquals(
            first=target[path],
            second=actual
        )


class DeployActionTestRunInvalidAccess(unittest.TestCase):
    def test_run_invalid_owner(self) -> None:
        def _file_factory(path: str, invalid: str) -> UnixFile:
            return UnixFileMock(
                path=path,
                owner=invalid,
                group="",
                mode="",
                checksum="",
                ctime=""
            )

        def _action_factory(
            stat_cmd: UnixStatCmd,
            path: str,
            valid: str
        ) -> Action:
            return UnixDeployAction(
                path=path,
                owner=valid,
                group="",
                mode="",
                pre_cmd=PreCmd(),
                deploy_cmd=UnixDeployCmd(),
                stat_cmd=stat_cmd,
                action_result_factory=ActionResultFactory()
            )

        self._test(
            err_msg_template="Invalid owner: [%s] instead of [%s]",
            file_factory=_file_factory,
            action_factory=_action_factory
        )

    def test_run_invalid_group(self) -> None:
        def _file_factory(path: str, invalid: str) -> UnixFile:
            return UnixFileMock(
                path=path,
                owner="",
                group=invalid,
                mode="",
                checksum="",
                ctime=""
            )

        def _action_factory(
            stat_cmd: UnixStatCmd,
            path: str,
            valid: str
        ) -> Action:
            return UnixDeployAction(
                path=path,
                owner="",
                group=valid,
                mode="",
                pre_cmd=PreCmd(),
                deploy_cmd=UnixDeployCmd(),
                stat_cmd=stat_cmd,
                action_result_factory=ActionResultFactory()
            )

        self._test(
            err_msg_template="Invalid group: [%s] instead of [%s]",
            file_factory=_file_factory,
            action_factory=_action_factory
        )

    def test_run_invalid_mode(self) -> None:
        def _file_factory(path: str, invalid: str) -> UnixFile:
            return UnixFileMock(
                path=path,
                owner="",
                group="",
                mode=invalid,
                checksum="",
                ctime=""
            )

        def _action_factory(
            stat_cmd: UnixStatCmd,
            path: str,
            valid: str
        ) -> Action:
            return UnixDeployAction(
                path=path,
                owner="",
                group="",
                mode=valid,
                pre_cmd=PreCmd(),
                deploy_cmd=UnixDeployCmd(),
                stat_cmd=stat_cmd,
                action_result_factory=ActionResultFactory()
            )

        self._test(
            err_msg_template="Invalid mode: [%s] instead of [%s]",
            file_factory=_file_factory,
            action_factory=_action_factory
        )

    def _test(
        self,
        err_msg_template: str,
        file_factory: Callable[[str, str], UnixFile],
        action_factory: Callable[[UnixStatCmd, str, str], Action]
    ) -> None:
        text_generator = UniqueTextGenerator()

        path = text_generator.next()
        valid = text_generator.next()
        invalid = text_generator.next()

        stat_cmd = UnixStatCmdMock(
            files={
                path: file_factory(path, invalid)
            }
        )

        action = action_factory(stat_cmd, path, valid)

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            action.run(TreeNodeImpl.empty())

        self.assertEqual(
            first=err_msg_template % (invalid, valid),
            second=str(err_ctx.exception)
        )


if (__name__ == "__main__"):
    unittest.main()
