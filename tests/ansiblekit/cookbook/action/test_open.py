import unittest

import pygentreetest.node

from pygentest.generator import UniqueValueGenerator
from pygentree.node.abstract import TreeNode
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.cookbook.action.open import NotUseTheSameCookbookTwiceException
from ansiblekit.cookbook.action.open import OpenAction
from ansiblekit.cookbook.action.open import OtherCookbookAlreadyOpenedException
from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.name.abstract import NameFactory
from ansiblekit.cookbook.repo.local.abstract import LocalRepo
from ansiblekit.cookbook.repo.local.abstract import LocalStoreFactory
from ansiblekit.cookbook.repo.remote.abstract import RemoteStoreFactory
from ansiblekit.cookbook.state.abstract import State
from ansiblekit.cookbook.state.abstract import StateFactory
from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.facts import Facts
from tests.ansiblekit.cookbook.action.mock import ActionResultFactoryMock
from tests.ansiblekit.cookbook.name.mock import NameFactoryMock
from tests.ansiblekit.cookbook.repo.local.mock import LocalStoreFactoryMock
from tests.ansiblekit.cookbook.repo.remote.mock import RemoteStoreFactoryMock
from tests.ansiblekit.cookbook.state.mock import StateFactoryMock


class OpenActionTestRunExceptions(unittest.TestCase):
    def test_run_not_use_the_same_cookbook_twice(self) -> None:
        local_store_factory = LocalStoreFactoryMock()
        remote_store_factory = RemoteStoreFactoryMock()
        name_factory = NameFactoryMock()
        action_result_factory = ActionResultFactoryMock()

        name = name_factory.random()
        action = OpenAction(
            uri=name.uri(),
            local_store_factory=local_store_factory,
            remote_store_factory=remote_store_factory,
            name_factory=name_factory,
            action_result_factory=action_result_factory
        )

        local_repo = local_store_factory.create()

        action.run(TreeNodeImpl.create(local_repo.facts()))

        err_ctx = self.assertRaises(NotUseTheSameCookbookTwiceException)
        with (err_ctx):
            action.run(TreeNodeImpl.create(local_repo.facts()))

        err_msg_template = "Not use the same cookbook twice [%s]"
        err_msg_vars = (name.uri())
        self.assertEqual(
            first=err_msg_template % err_msg_vars,
            second=str(err_ctx.exception)
        )

    def test_run_other_cookbook_already_opened(self) -> None:
        local_store_factory = LocalStoreFactoryMock()
        remote_store_factory = RemoteStoreFactoryMock()
        name_factory = NameFactoryMock()
        action_result_factory = ActionResultFactoryMock()
        name_generator: UniqueValueGenerator[Name] = UniqueValueGenerator(
            factory=name_factory.random
        )

        first_action_name = name_generator.next()
        first_action = OpenAction(
            uri=first_action_name.uri(),
            local_store_factory=local_store_factory,
            remote_store_factory=remote_store_factory,
            name_factory=name_factory,
            action_result_factory=action_result_factory
        )

        first_action_result = first_action.run(TreeNodeImpl.empty())

        second_action_name = name_generator.next()
        second_action = OpenAction(
            uri=second_action_name.uri(),
            local_store_factory=local_store_factory,
            remote_store_factory=remote_store_factory,
            name_factory=name_factory,
            action_result_factory=action_result_factory
        )

        err_ctx = self.assertRaises(OtherCookbookAlreadyOpenedException)
        with (err_ctx):
            second_action.run(first_action_result.facts())

        err_msg_template = "Other cookbook already opened [%s]"
        err_msg_vars = (first_action_name.uri())
        self.assertEqual(
            first=err_msg_template % err_msg_vars,
            second=str(err_ctx.exception)
        )


class OpenActionTestRun(unittest.TestCase):
    def test_run_remote_cookbook_not_exists(self) -> None:
        state_factory = StateFactoryMock()
        local_store_factory = LocalStoreFactoryMock()
        remote_store_factory = RemoteStoreFactoryMock(state_factory)
        name_factory = NameFactoryMock()
        action_result_factory = ActionResultFactoryMock()

        name = name_factory.random()
        action = OpenAction(
            uri=name.uri(),
            local_store_factory=local_store_factory,
            remote_store_factory=remote_store_factory,
            name_factory=name_factory,
            action_result_factory=action_result_factory
        )

        local_store = local_store_factory.create()

        self._test(
            result=action.run(TreeNodeImpl.create(local_store.facts())),
            local_repo=local_store.open(name),
            state_factory=state_factory,
            name_factory=name_factory,
            name=name,
            changed=True,
            state=state_factory.create()
        )

    def test_run_remote_cookbook_exists(self) -> None:
        state_factory = StateFactoryMock()
        local_store_factory = LocalStoreFactoryMock()
        remote_store_factory = RemoteStoreFactoryMock(state_factory)
        name_factory = NameFactoryMock()
        action_result_factory = ActionResultFactoryMock()

        name = name_factory.random()
        action = OpenAction(
            uri=name.uri(),
            local_store_factory=local_store_factory,
            remote_store_factory=remote_store_factory,
            name_factory=name_factory,
            action_result_factory=action_result_factory
        )

        remote_store = remote_store_factory.create()
        local_store = local_store_factory.create()

        facts: Facts = {
            **remote_store.facts(),
            **local_store.facts()
        }

        state = state_factory.random()
        remote_repo = remote_store.create(name)

        remote_repo.push(state)

        self._test(
            result=action.run(TreeNodeImpl.create(facts)),
            local_repo=local_store.open(name),
            state_factory=state_factory,
            name_factory=name_factory,
            name=name,
            changed=False,
            state=state
        )

    def _test(
        self,
        result: ActionResult,
        local_repo: LocalRepo,
        state_factory: StateFactory,
        name_factory: NameFactory,
        name: Name,
        changed: bool,
        state: State
    ) -> None:
        self.assertEqual(
            first=name,
            second=name_factory.load(result.facts())
        )

        self.assertEqual(
            first=changed,
            second=result.changed()
        )

        self.assertEqual(
            first=state,
            second=local_repo.fetch_initial_state()
        )

        self.assertEqual(
            first=state_factory.create(),
            second=local_repo.fetch_actual_state()
        )


class OpenActionTest(unittest.TestCase):
    def test_create_unsupported_args(self) -> None:
        def _callback(args: TreeNode) -> None:
            OpenAction.create(
                args=args,
                local_store_factory=LocalStoreFactory(),
                remote_store_factory=RemoteStoreFactory(),
                name_factory=NameFactory(),
                action_result_factory=ActionResultFactory()
            )

        pygentreetest.node.test_unsupported_keys(
            test_case=self,
            nodes={
                "uri": ""
            },
            callback=_callback
        )


if (__name__ == "__main__"):
    unittest.main()
