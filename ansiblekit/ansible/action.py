from __future__ import annotations

from typing import Dict
from typing import Generic
from typing import Optional
from typing import TypeVar
from typing import cast

from ansible.playbook.play_context import PlayContext
from ansible.playbook.task import Task
from ansible.plugins.action import ActionBase
from ansible.plugins.connection import ConnectionBase
from ansible.template import Templar
from pygenerr.expected import ExpectedException
from pygentree.node.abstract import TreeNode
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.ansible.result import ActionException
from ansiblekit.ansible.result import ActionResult

Host = TypeVar("Host")


class Action():
    def run(self, facts: TreeNode) -> ActionResult: ...


class _ActionLoaderTyping:
    def get(
        self,
        name: str,
        task: Task,
        connection: object,
        play_context: object,
        loader: object,
        templar: object,
        shared_loader_obj: _SharedLoaderObjTyping
    ) -> _ActionBaseTyping: ...


class _SharedLoaderObjTyping:
    action_loader: _ActionLoaderTyping = _ActionLoaderTyping()


class _ActionBaseTyping:
    _shared_loader_obj: _SharedLoaderObjTyping = _SharedLoaderObjTyping()

    def _execute_module(
        self,
        module_name: str,
        module_args: Dict[str, object],
        tmp: Optional[object],
        task_vars: Optional[object],
        persist_files: object = None,
        delete_remote_tmp: object = None,
        wrap_async: object = None
    ) -> Dict[str, object]: ...

    def run(
        self,
        tmp: Optional[object] = None,
        task_vars: Optional[object] = None
    ) -> Dict[str, object]: ...


class _AnsibleActionBase(ActionBase, _ActionBaseTyping):  # type: ignore
    def execute_module(
        self,
        module_name: str,
        module_args: Dict[str, object],
        tmp: Optional[object],
        task_vars: Optional[object]
    ) -> Dict[str, object]:
        wrapper = cast(_ActionBaseTyping, self)

        return wrapper._execute_module(
            module_name=module_name,
            module_args=module_args,
            tmp=tmp,
            task_vars=task_vars
        )

    def new_task(self) -> Task:
        return self._task.copy()

    def get_connection(self) -> ConnectionBase:
        return self._connection

    def get_play_context(self) -> PlayContext:
        return self._play_context

    def get_templar(self) -> Templar:
        return self._templar

    def get_shared_loader_obj(self) -> _SharedLoaderObjTyping:
        wrapper = cast(_ActionBaseTyping, self)

        return wrapper._shared_loader_obj


class AnsibleAction(Generic[Host], _AnsibleActionBase):
    def run(
        self,
        tmp: Optional[object] = None,
        task_vars: Optional[object] = None
    ) -> ActionResult:
        args = TreeNodeImpl.create(self._task.args)
        facts = TreeNodeImpl.create(task_vars)
        executor = AnsibleExecutor(
            action=self,
            tmp=tmp,
            task_vars=task_vars
        )
        host = self.create_host(executor)

        try:
            action = self.create_action(args, host)

            return action.run(facts)
        except ExpectedException as e:
            return ActionResult.failed(e)

    def create_action(self, args: TreeNode, host: Host) -> Action: ...
    def create_host(self, executor: AnsibleExecutor) -> Host: ...


class AnsibleExecutor:
    action: _AnsibleActionBase
    task_vars: Optional[object]

    def __init__(
        self,
        action: _AnsibleActionBase,
        tmp: Optional[object],
        task_vars: Optional[object]
    ) -> None:
        self.action = action
        self.tmp = tmp
        self.task_vars = task_vars

    def execute_module(
        self,
        name: str,
        args: Dict[str, object]
    ) -> ActionResult:
        result = ActionResult(
            self.action.execute_module(
                module_name=name,
                module_args=args,
                tmp=self.tmp,
                task_vars=self.task_vars
            )
        )

        return self.process_result(result)

    def execute_action(
        self,
        name: str,
        args: Dict[str, object]
    ) -> ActionResult:
        task = self.action.new_task()
        variable_manager = task.get_variable_manager()
        loader = task.get_loader()
        connection = self.action.get_connection()
        play_context = self.action.get_play_context()
        templar = self.action.get_templar()
        shared_loader_obj = self.action.get_shared_loader_obj()
        action_loader = shared_loader_obj.action_loader

        task.load_data(
            ds={
                "action": name,
                "args": args
            },
            variable_manager=variable_manager,
            loader=loader
        )

        action = action_loader.get(
            name=name,
            task=task,
            connection=connection,
            play_context=play_context,
            loader=loader,
            templar=templar,
            shared_loader_obj=shared_loader_obj
        )

        result = ActionResult(
            action.run(self.tmp, self.task_vars)
        )

        return self.process_result(result)

    def process_result(self, result: ActionResult) -> ActionResult:
        if (result.get("failed", False) is False):
            return result
        else:
            raise ActionException(result)
