from __future__ import annotations

from typing import Dict

from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager

class Task:
    args: Dict[str, object]

    def copy(self) -> Task: ...

    def get_variable_manager(self) -> VariableManager: ...

    def get_loader(self) -> DataLoader: ...

    def load_data(
        self,
        ds: Dict[str, object],
        variable_manager: VariableManager,
        loader: DataLoader
    ) -> None: ...
