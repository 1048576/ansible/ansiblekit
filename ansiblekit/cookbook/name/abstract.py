from __future__ import annotations

from pygenerr.expected import ExpectedException
from pygentree.node.abstract import TreeNode

from ansiblekit.core.facts import Facts


class CookbookNotSpecifiedException(ExpectedException):
    def __init__(self) -> None:
        super().__init__("Cookbook not specified")


class Name:
    def uri(self) -> str: ...

    def facts(self) -> Facts: ...


class NameFactory:
    @staticmethod
    def empty() -> Name: ...

    @staticmethod
    def create(uri: str) -> Name: ...

    @staticmethod
    def load(facts: TreeNode) -> Name: ...
