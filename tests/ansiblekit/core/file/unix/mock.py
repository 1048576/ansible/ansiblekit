from ansiblekit.cookbook.state.file.abstract import StateFile
from ansiblekit.core.file.unix.abstract import UnixFile


class UnixFileMock(UnixFile):
    _path: str
    _owner: str
    _group: str
    _mode: str
    _checksum: str
    _ctime: str

    def __init__(
        self,
        path: str,
        owner: str,
        group: str,
        mode: str,
        checksum: str,
        ctime: str
    ) -> None:
        self._path = path
        self._owner = owner
        self._group = group
        self._mode = mode
        self._checksum = checksum
        self._ctime = ctime

    def __eq__(self, o: object) -> bool:
        if (isinstance(o, StateFile)):
            if (o.checksum() != self._checksum):
                return False

            if (o.ctime() != self._ctime):
                return False

            return True
        else:
            return (o == self)

    def path(self) -> str:
        return self._path

    def owner(self) -> str:
        return self._owner

    def group(self) -> str:
        return self._group

    def mode(self) -> str:
        return self._mode

    def checksum(self) -> str:
        return self._checksum

    def ctime(self) -> str:
        return self._ctime
