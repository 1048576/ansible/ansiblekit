from __future__ import annotations

from pygenerr.expected import ExpectedException
from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.cmd.unix.deploy.abstract import UnixDeployCmd
from ansiblekit.core.action.abstract import Action
from ansiblekit.core.action.abstract import ActionResult
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.cmd.unix.stat.abstract import UnixStatCmd


class UnixDeployAction(Action):
    _path: str
    _owner: str
    _group: str
    _mode: str
    _pre_cmd: PreCmd
    _deploy_cmd: UnixDeployCmd
    _stat_cmd: UnixStatCmd
    _action_result_factory: ActionResultFactory

    def __init__(
        self,
        path: str,
        owner: str,
        group: str,
        mode: str,
        pre_cmd: PreCmd,
        deploy_cmd: UnixDeployCmd,
        stat_cmd: UnixStatCmd,
        action_result_factory: ActionResultFactory
    ) -> None:
        self._path = path
        self._owner = owner
        self._group = group
        self._mode = mode
        self._pre_cmd = pre_cmd
        self._deploy_cmd = deploy_cmd
        self._stat_cmd = stat_cmd
        self._action_result_factory = action_result_factory

    def run(self, facts: TreeNode) -> ActionResult:
        pre_cmd_result = self._pre_cmd.run(facts)

        self._deploy_cmd.run(
            path=self._path,
            owner=self._owner,
            group=self._group,
            mode=self._mode
        )

        file = self._stat_cmd.run(self._path)

        if (file.owner() != self._owner):
            raise ExpectedException(
                msg_template="Invalid owner: [%s] instead of [%s]",
                msg_vars=[file.owner(), self._owner]
            )

        if (file.group() != self._group):
            raise ExpectedException(
                msg_template="Invalid group: [%s] instead of [%s]",
                msg_vars=[file.group(), self._group]
            )

        if (file.mode() != self._mode):
            raise ExpectedException(
                msg_template="Invalid mode: [%s] instead of [%s]",
                msg_vars=[file.mode(), self._mode]
            )

        local_repo = pre_cmd_result.local_repo()
        initial_state = local_repo.fetch_initial_state()
        previous_state = local_repo.fetch_actual_state()

        update_state_result = previous_state.update_file(
            path=file.path(),
            checksum=file.checksum(),
            ctime=file.ctime(),
            state=initial_state
        )

        local_repo.push(update_state_result.state())

        return self._action_result_factory.success(
            changed=update_state_result.changed(),
            facts={}
        )
