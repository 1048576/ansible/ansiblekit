import unittest

from typing import Callable

import pygentreetest.node

from pygentree.node.abstract import TreeNode
from pygentree.node.impl import TreeNodeImpl

from ansiblekit.cookbook.action.close import CloseAction
from ansiblekit.cookbook.cmd.pre.abstract import PreCmd
from ansiblekit.cookbook.name.abstract import CookbookNotSpecifiedException
from ansiblekit.cookbook.name.abstract import NameFactory
from ansiblekit.cookbook.repo.remote.abstract import RemoteRepo
from ansiblekit.cookbook.repo.remote.abstract import RemoteStoreFactory
from ansiblekit.cookbook.state.abstract import State
from ansiblekit.core.action.abstract import ActionResultFactory
from ansiblekit.core.facts import Facts
from tests.ansiblekit.cookbook.action.mock import ActionResultFactoryMock
from tests.ansiblekit.cookbook.cmd.pre.mock import PreCmdMock
from tests.ansiblekit.cookbook.name.mock import NameFactoryMock
from tests.ansiblekit.cookbook.repo.remote.mock import RemoteStoreFactoryMock
from tests.ansiblekit.cookbook.state.mock import StateFactoryMock


class CloseActionTestRun(unittest.TestCase):
    def test_run_cookbook_not_exists(self) -> None:
        def _callback(
            state_factory: StateFactoryMock,
            remote_repo: RemoteRepo,
            state: State
        ) -> None:
            pass

        self._test(True, _callback)

    def test_run_cookbook_changed(self) -> None:
        def _callback(
            state_factory: StateFactoryMock,
            remote_repo: RemoteRepo,
            state: State
        ) -> None:
            remote_repo.push(state_factory.random())

        self._test(True, _callback)

    def test_run_cookbook_not_changed(self) -> None:
        def _callback(
            state_factory: StateFactoryMock,
            remote_repo: RemoteRepo,
            state: State
        ) -> None:
            remote_repo.push(state)

        self._test(False, _callback)

    def _test(
        self,
        expected_changed: bool,
        callback: Callable[[StateFactoryMock, RemoteRepo, State], None]
    ) -> None:
        state_factory = StateFactoryMock()
        remote_store_factory = RemoteStoreFactoryMock(state_factory)
        name_factory = NameFactoryMock()
        action_result_factory = ActionResultFactoryMock()

        name = name_factory.random()
        remote_store = remote_store_factory.create()
        remote_repo = remote_store.create(name)
        expected_state = StateFactoryMock.random()

        callback(state_factory, remote_repo, expected_state)

        action = CloseAction(
            remote_store_factory=remote_store_factory,
            pre_cmd=PreCmdMock(
                name=name,
                initial_state=state_factory.random(),
                actual_state=expected_state
            ),
            name_factory=name_factory,
            action_result_factory=action_result_factory
        )

        facts: Facts = {
            **remote_store.facts(),
            **name.facts()
        }

        result = action.run(TreeNodeImpl.create(facts))

        self.assertEqual(
            first=expected_changed,
            second=result.changed()
        )

        err_ctx = self.assertRaises(CookbookNotSpecifiedException)
        with (err_ctx):
            name_factory.load(result.facts())

        self.assertEqual(
            first=expected_state,
            second=remote_repo.fetch()
        )


class CloseActionTest(unittest.TestCase):
    def test_create_unsupported_args(self) -> None:
        def _callback(args: TreeNode) -> None:
            CloseAction.create(
                args=args,
                remote_store_factory=RemoteStoreFactory(),
                pre_cmd=PreCmd(),
                name_factory=NameFactory(),
                action_result_factory=ActionResultFactory()
            )

        pygentreetest.node.test_unsupported_keys(
            test_case=self,
            nodes={},
            callback=_callback
        )


if (__name__ == "__main__"):
    unittest.main()
