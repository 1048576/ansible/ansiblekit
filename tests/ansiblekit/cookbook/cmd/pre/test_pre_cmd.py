import unittest

from pygentree.node.impl import TreeNodeImpl

from ansiblekit.cookbook.cmd.pre.abstract import CookbookNotOpenException
from ansiblekit.cookbook.cmd.pre.impl import PreCmdImpl
from ansiblekit.cookbook.name.abstract import NameFactory
from ansiblekit.cookbook.repo.local.abstract import LocalStoreFactory
from ansiblekit.core.facts import Facts
from tests.ansiblekit.cookbook.name.mock import NameFactoryMock
from tests.ansiblekit.cookbook.repo.local.mock import LocalStoreFactoryMock


class PreCmdTestRunCookbookNotOpen(unittest.TestCase):
    def test_run_cookbook_not_open_when_local_repo_not_exists(self) -> None:
        local_store_factory = LocalStoreFactoryMock()
        name_factory = NameFactoryMock()

        self._test(
            local_store_factory=local_store_factory,
            name_factory=name_factory,
            facts=name_factory.random().facts()
        )

    def test_run_cookbook_not_open_when_name_not_specified(self) -> None:
        local_store_factory = LocalStoreFactoryMock()
        name_factory = NameFactoryMock()

        self._test(
            local_store_factory=local_store_factory,
            name_factory=name_factory,
            facts=local_store_factory.create().facts()
        )

    def _test(
        self,
        local_store_factory: LocalStoreFactory,
        name_factory: NameFactory,
        facts: Facts
    ) -> None:
        pre_cmd = PreCmdImpl(
            local_store_factory=local_store_factory,
            name_factory=name_factory
        )

        err_ctx = self.assertRaises(CookbookNotOpenException)
        with (err_ctx):
            pre_cmd.run(TreeNodeImpl.create(facts))

        self.assertEqual(
            first="Cookbook not open",
            second=str(err_ctx.exception)
        )


if (__name__ == "__main__"):
    unittest.main()
