from typing import Dict

from ansiblekit.cookbook.cmd.unix.deploy.abstract import UnixDeployCmd
from ansiblekit.cookbook.state.file.abstract import StateFile
from ansiblekit.core.file.unix.abstract import UnixFile
from tests.ansiblekit.core.file.unix.mock import UnixFileMock


class UnixDeployCmdMock(UnixDeployCmd):
    _source: Dict[str, StateFile]
    _target: Dict[str, UnixFile]

    def __init__(
        self,
        source: Dict[str, StateFile],
        target: Dict[str, UnixFile]
    ) -> None:
        self._source = source
        self._target = target

    def run(self, path: str, owner: str, group: str, mode: str) -> None:
        file = self._source.pop(path)

        self._target[path] = UnixFileMock(
            path=path,
            owner=owner,
            group=group,
            mode=mode,
            checksum=file.checksum(),
            ctime=file.ctime()
        )
