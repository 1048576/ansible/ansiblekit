from pygentree.node.abstract import TreeNode

from ansiblekit.core.facts import Facts


class StateFile:
    def checksum(self) -> str: ...

    def ctime(self) -> str: ...

    def facts(self) -> Facts: ...


class StateFileFactory:
    def create(self, checksum: str, ctime: str) -> StateFile: ...

    def load(self, facts: TreeNode) -> StateFile: ...
