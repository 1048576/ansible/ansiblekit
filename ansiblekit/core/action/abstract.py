from pygentree.node.abstract import TreeNode

from ansiblekit.core.facts import Facts


class ActionResult:
    def changed(self) -> bool: ...

    def facts(self) -> TreeNode: ...


class ActionResultFactory:
    def success(
        self,
        changed: bool,
        facts: Facts
    ) -> ActionResult: ...


class Action:
    def run(self, facts: TreeNode) -> ActionResult: ...
