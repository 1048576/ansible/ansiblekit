from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.state.file.abstract import StateFile
from ansiblekit.cookbook.state.file.abstract import StateFileFactory
from ansiblekit.core.facts import Facts


class StateFileImpl(StateFile):
    KEY_CHECKSUM: str = "checksum"
    KEY_CTIME: str = "ctime"

    @staticmethod
    def load(facts: TreeNode) -> StateFile:
        with (facts):
            return StateFileImpl(
                checksum=facts.fetch_text(StateFileImpl.KEY_CHECKSUM),
                ctime=facts.fetch_text(StateFileImpl.KEY_CTIME)
            )

    _checksum: str
    _ctime: str

    def __init__(self, checksum: str, ctime: str) -> None:
        self._checksum = checksum
        self._ctime = ctime

    def __eq__(self, o: object) -> bool:
        if (not isinstance(o, StateFileImpl)):
            return False
        if (self._checksum != o._checksum):
            return False
        if (self._ctime != o._ctime):
            return False

        return True

    def checksum(self) -> str:
        return self._checksum

    def ctime(self) -> str:
        return self._ctime

    def facts(self) -> Facts:
        return {
            StateFileImpl.KEY_CHECKSUM: self._checksum,
            StateFileImpl.KEY_CTIME: self._ctime
        }


class StateFileFactoryImpl(StateFileFactory):
    def create(self, checksum: str, ctime: str) -> StateFile:
        return StateFileImpl(
            checksum=checksum,
            ctime=ctime
        )

    def load(self, facts: TreeNode) -> StateFile:
        return StateFileImpl.load(facts)
