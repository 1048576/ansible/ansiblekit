from pygenerr.expected import ExpectedException
from pygentree.node.abstract import TreeNode

from ansiblekit.cookbook.name.abstract import Name
from ansiblekit.cookbook.repo.local.abstract import LocalRepo


class CookbookNotOpenException(ExpectedException):
    def __init__(self) -> None:
        super().__init__("Cookbook not open")


class PreCmdResult:
    _local_repo: LocalRepo
    _name: Name

    def __init__(
        self,
        local_repo: LocalRepo,
        name: Name
    ) -> None:
        self._local_repo = local_repo
        self._name = name

    def local_repo(self) -> LocalRepo:
        return self._local_repo

    def name(self) -> Name:
        return self._name


class PreCmd:
    def run(self, facts: TreeNode) -> PreCmdResult: ...
